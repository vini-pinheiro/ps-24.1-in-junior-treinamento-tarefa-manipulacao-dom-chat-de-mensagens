let btnEnviar = document.querySelector("#enviar");

btnEnviar.addEventListener("click", registrarMensagem);

let texto = document.querySelector('#inputMensagem');
texto.addEventListener('input', () => {
    autoAjustarAlturaTextArea(texto);
})

function verificarMensagemVazia(texto) {
    if(texto == "") return true;
}

function registrarMensagem() {
    let texto = document.getElementById("inputMensagem").value;
    if(verificarMensagemVazia(texto)) return ;
    document.getElementById("inputMensagem").value= "";
    mostrarMensagem(texto);
}

function mostrarMensagem(texto) {
    let main = document.querySelector("main");
    criarElementosDaMensagem(texto, main);
}

function estilizarTextArea(elemento){
    elemento.style.width = "450px";
    elemento.style.borderRadius = "2rem 2rem 0 2rem";
    elemento.style.padding = "0.5rem";
    elemento.style.resize = "none";
}

function criarElementosDaMensagem(texto, main){

    let escopo = document.createElement("article");
    escopo.style.display = "flex";
    escopo.style.flexDirection = "column";
    escopo.style.alignItems = "flex-end";
    escopo.style.paddingBottom = "2rem";

    
    let textArea = document.createElement("textarea");
    textArea.setAttribute("disabled", "");
    textArea.classList.add("texto-enviado");
    textArea.value = texto;
    estilizarTextArea(textArea);
    console.log(textArea.offsetHeight)


    let divBtn = document.createElement("div");
    divBtn.classList.add("botoesMensagemEnviada");
    escopo.style.display = "flex";
    divBtn.style.justifyContent = "right";

    let btnEditar = document.createElement("button");
    btnEditar.innerHTML = "Editar";
    btnEditar.classList.add("editar");

    let btnExcluir = document.createElement("button");
    btnExcluir.innerHTML = "Excluir";
    btnExcluir.classList.add("excluir");

    let btnSalvar = document.createElement("button");
    btnSalvar.innerHTML = "Salvar";
    btnSalvar.classList.add("salvar");

    divBtn.append(btnEditar);
    divBtn.append(btnExcluir);

    escopo.append(textArea);
    escopo.append(divBtn);

    main.append(escopo);


    btnEditar.addEventListener('click', ()=>{
        btnEditar.replaceWith(btnSalvar);
        textArea.removeAttribute('disabled')
    })
    btnSalvar.addEventListener('click', () => {
        btnSalvar.replaceWith(btnEditar);
        textArea.setAttribute('disabled', '');
    })

    btnExcluir.addEventListener('click', () => {
        escopo.replaceWith('');
    })


}



function autoAjustarAlturaTextArea(elemento) {
    elemento.style.height = '2rem';

    if((elemento.scrollHeight > 92)) 
    {
        elemento.style.height='92px';
        elemento.style.overflow = 'auto';
    }
    else 
    {
        elemento.style.height = elemento.scrollHeight + 'px';
        elemento.style.overflow = 'hidden';
    }
  }
